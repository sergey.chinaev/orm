package genstorage

import (
	"flag"
	"github.com/stretchr/testify/assert"
	"os"
	"path/filepath"
	"testing"
	"text/template"
)

func TestCreateStorageFiles(t *testing.T) {
	// Создаем два шаблона для тестирования
	emptyTemplate := template.New("mock")
	mockTemplate := template.New("mock")
	err, _ := mockTemplate.Parse("Mock template")
	if err != nil {
		return
	}
	// Определяем структуру для хранения полей Storage
	type fields struct {
		FileName          string
		StorageTemplate   *template.Template
		InterfaceTemplate *template.Template
		TemplateData      TemplateData
	}
	// Определяем тестовые случаи
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
		setup   func()
	}{
		{
			name: "valid data",
			fields: fields{
				FileName:          "test_model",
				StorageTemplate:   mockTemplate,
				InterfaceTemplate: mockTemplate,
				TemplateData:      TemplateData{},
			},
			wantErr: false,
			setup: func() {
				err := os.MkdirAll("../storage/", 0755)
				if err != nil {
					return
				}
				file, _ := os.Create("../storage/test_model_interface.go")
				file.Close()
				file2, _ := os.Create("../storage/test_model_storage.go")
				file2.Close()
			},
		},
		{
			name: "wrong template",
			fields: fields{
				FileName:          "test_model",
				StorageTemplate:   emptyTemplate,
				InterfaceTemplate: emptyTemplate,
				TemplateData:      TemplateData{},
			},
			wantErr: true,
		},
		{
			name: "files already exists",
			fields: fields{
				FileName:          "test_model",
				StorageTemplate:   mockTemplate,
				InterfaceTemplate: mockTemplate,
				TemplateData:      TemplateData{},
			},
			setup: func() {
				err := os.MkdirAll("../storage/", 0755)
				if err != nil {
					return
				}
				file, _ := os.Create("../storage/test_model_interface.go")
				file.Close()
			},
			wantErr: true,
		},
	}

	// Проходим по каждому тестовому случаю
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Выполняем функцию setup, если она определена
			if tt.setup != nil {
				tt.setup()
				// Удаляем директорию после теста
				defer os.RemoveAll("../storage/")
			}
			// Создаем объект Storage для тестирования
			s := &Storage{
				FileName:          tt.fields.FileName,
				StorageTemplate:   tt.fields.StorageTemplate,
				InterfaceTemplate: tt.fields.InterfaceTemplate,
				TemplateData:      tt.fields.TemplateData,
			}
			// Вызываем функцию CreateStorageFiles и проверяем ошибку
			if err := s.CreateStorageFiles(); (err != nil) != tt.wantErr {
				files := []string{
					filepath.Join("../repository/", s.FileName+"_storage.go"),
					filepath.Join("../repository/", s.FileName+"_interface.go"),
				}
				for _, f := range files {
					if _, err := os.Stat(f); os.IsNotExist(err) {
						t.Errorf("CreateStorageFiles() error = %v, wantErr %v", err, tt.wantErr)
					}
				}

			}
			// Проверяем создание файлов, если ошибок не ожидалось
			if !tt.wantErr {
				for _, filepath := range []string{
					"../storage/" + tt.fields.FileName + "_storage.go",
					"../storage/" + tt.fields.FileName + "_interface.go",
				} {
					if _, err := os.Stat(filepath); os.IsNotExist(err) {
						t.Errorf("File %s is not created", filepath)
					}
				}
			}
		})
	}
}

func TestGetTableName(t *testing.T) {
	type args struct {
		fileName string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "not valid method name",
			args:    args{fileName: "wrong_model.go"},
			want:    "",
			wantErr: false,
		},
		{
			name:    "empty model",
			args:    args{fileName: "empty_model.go"},
			want:    "",
			wantErr: false,
		},
		{
			name:    "not valid file name",
			args:    args{fileName: ""},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetTableName(tt.args.fileName)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTableName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetTableName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetStructName(t *testing.T) {
	type args struct {
		fileName string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "empty struct model",
			args:    args{fileName: "empty_model.go"},
			want:    "",
			wantErr: false,
		},
		{
			name:    "not valid file name",
			args:    args{fileName: "..."},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetStructName(tt.args.fileName)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetStructName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetStructName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetFileName(t *testing.T) {
	tests := []struct {
		name      string
		flagValue string
		want      string
		wantErr   bool
	}{
		{
			name:      "valid flag",
			flagValue: "testfile.go",
			want:      "testfile.go",
			wantErr:   false,
		},
		{
			name:      "empty flag",
			flagValue: "",
			want:      "",
			wantErr:   true,
		},
	}
	for _, tt := range tests {
		err := flag.Set("filename", tt.flagValue)
		if err != nil {
			return
		}
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetFileName()
			if err != nil {
				t.Errorf("GetFileName() got = %v, err %v", got, err)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFileName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetFileName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetPackageName(t *testing.T) {
	type args struct {
		fileName string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "valid file",
			args:    args{fileName: "test_file.go"},
			want:    "genstorage",
			wantErr: false,
		},
		//{
		//	name:    "file without package name",
		//	args:    args{fileName: "no_package.go"},
		//	want:    "",
		//	wantErr: false,
		//},
		{
			name:    "non-existent file",
			args:    args{fileName: "nonexistent.go"},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetPackageName(tt.args.fileName)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetPackageName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetPackageName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewStorageTemplate(t *testing.T) {
	testModule := "test module"
	testPath := "test path"
	tmpl, err := NewStorageTemplate(testModule, testPath)
	if err != nil {
		t.Errorf("NewStorageTemplate returned an error: %v", err)
	}

	assert.NotNil(t, tmpl)
}

func TestNewInterfaceTemplate(t *testing.T) {
	tmpl, err := NewInterfaceTemplate()
	if err != nil {
		t.Errorf("NewInterfaceTemplate returned an error: %v", err)
	}

	assert.NotNil(t, tmpl)
}
