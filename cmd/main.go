package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"strings"

	_ "gitlab.com/golight/dao/params"
	"gitlab.com/golight/orm/genstorage"
)

// ./generate -filename="../genstore/models/test_model.go" - создаем директорию с файлами хранилища в корневом каталоге
//
//go:generate go build -o ~/go/bin

func main() {
	var err error

	fmt.Println("v1.0.1")

	var filename string
	filename, err = genstorage.GetFileName()
	if err != nil {
		return
	}
	fmt.Println(filename)

	err = GenerateMethods(filename)
	if err != nil {
		log.Fatal(err)
	}
	module, path, err := genstorage.FindGoModFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	var storage *genstorage.Storage
	storage, err = genstorage.NewStorage(filename, module, path)
	if err != nil {
		log.Fatal(err)
	}

	err = storage.CreateStorageFiles()
	if err != nil {
		log.Fatal(err)
	}
}

type FieldInfo struct {
	Name string
	Type string
}

type MethodInfo struct {
	Receiver string
	Name     string
}

type ReflectData struct {
	StructName string
	HasDBTag   bool
	Fields     []FieldInfo
	Methods    []MethodInfo
}

func ReflectFile(fileName string) ([]ReflectData, error) {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, fileName, nil, parser.ParseComments)
	if err != nil {
		return nil, err
	}

	var reflectDataList []ReflectData

	ast.Inspect(node, func(n ast.Node) bool {
		switch t := n.(type) {
		case *ast.TypeSpec:
			var rData ReflectData
			rData.StructName = t.Name.Name

			s, ok := t.Type.(*ast.StructType)
			if ok {
				for _, field := range s.Fields.List {
					if len(field.Names) == 0 {
						continue
					}
					fieldName := field.Names[0].Name
					fieldType := fmt.Sprintf("%s", field.Type)
					if field.Tag != nil {
						tagValue := field.Tag.Value
						if strings.Contains(tagValue, "db:\"") {
							rData.HasDBTag = true
						}
					}
					rData.Fields = append(rData.Fields, FieldInfo{
						Name: fieldName,
						Type: fieldType,
					})
				}
			}

			reflectDataList = append(reflectDataList, rData)

		case *ast.FuncDecl:
			if t.Recv != nil && len(t.Recv.List) == 1 {
				starExpr, ok := t.Recv.List[0].Type.(*ast.StarExpr)
				if ok {
					ident, ok := starExpr.X.(*ast.Ident)
					if ok {
						// Update the corresponding ReflectData for this receiver
						for idx, rData := range reflectDataList {
							if rData.StructName == ident.Name {
								reflectDataList[idx].Methods = append(reflectDataList[idx].Methods, MethodInfo{
									Receiver: ident.Name,
									Name:     t.Name.Name,
								})
							}
						}
					}
				}
			}
		}
		return true
	})

	return reflectDataList, nil
}

func GenerateMethods(filename string) error {
	var err error
	_, err = os.Stat(filename)
	if os.IsExist(err) {
		return err
	}
	// Извлечение информации о структуре
	data, err := ReflectFile(filename)
	if err != nil {
		return err
	}
	// Генерация методов на основе извлеченной информации и вывод результата
	for _, st := range data {
		if !st.HasDBTag {
			continue
		}
		generatedCode := generateMethods(st)
		if err = appendToFile(filename, "\n"+generatedCode); err != nil {
			return err
		}
	}

	return nil
}

func generateMethods(data ReflectData) string {
	receiver := strings.ToLower(data.StructName[:1])
	builder := &strings.Builder{}

	if !MethodExists(data.Methods, data.StructName, "TableName") {
		// Генерация метода TableName
		fmt.Fprintf(builder, "func (%s *%s) TableName() string {\n", receiver, data.StructName)
		fmt.Fprintf(builder, "\treturn \"%ss\"\n", strings.ToLower(data.StructName))
		builder.WriteString("}\n\n")
	}

	if !MethodExists(data.Methods, data.StructName, "OnCreate") {
		// Генерация метода OnCreate
		fmt.Fprintf(builder, "func (%s *%s) OnCreate() []string {\n", receiver, data.StructName)
		builder.WriteString("\treturn []string{}\n")
		builder.WriteString("}\n\n")
	}

	if !MethodExists(data.Methods, data.StructName, "FieldsPointers") {
		// Генерация метода FieldsPointers
		fmt.Fprintf(builder, "func (%s *%s) FieldsPointers() []interface{} {\n", receiver, data.StructName)
		builder.WriteString("\treturn []interface{}{\n")
		for _, field := range data.Fields {
			fmt.Fprintf(builder, "\t\t&%s.%s,\n", receiver, field.Name)
		}
		builder.WriteString("\t}\n")
		builder.WriteString("}\n\n")
	}

	return builder.String()
}

func MethodExists(methods []MethodInfo, structName, methodName string) bool {
	for _, m := range methods {
		if m.Receiver == structName && m.Name == methodName {
			return true
		}
	}

	return false
}

func appendToFile(filename, content string) error {
	fmt.Println(content)
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err := f.WriteString(content); err != nil {
		return err
	}

	return nil
}
