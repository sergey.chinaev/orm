module gitlab.com/golight/orm

go 1.21

//toolchain go1.21.4

require (
	github.com/stretchr/testify v1.8.4
	gitlab.com/golight/dao v1.2.0
	gitlab.com/golight/scanner v1.2.0
	go.uber.org/zap v1.26.0
	golang.org/x/text v0.14.0
)

require (
	github.com/Masterminds/squirrel v1.5.4 // indirect
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	gitlab.com/golight/entity v1.0.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/xerrors v0.0.0-20220907171357-04be3eba64a2 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
