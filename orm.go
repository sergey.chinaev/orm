package orm

import (
	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/connector"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/scanner"

	"go.uber.org/zap"
)

func NewOrm(dbConf params.DB, scanner scanner.Scanner, logger *zap.Logger) (*dao.DAO, error) {
	var (
		orm *connector.SqlDB
		err error
	)

	orm, err = connector.NewSqlDB(dbConf, scanner, logger)
	if err != nil {
		logger.Fatal("error init db", zap.Error(err))
	}

	return orm.DAO, nil
}
