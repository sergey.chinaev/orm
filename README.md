# Orm

The Orm package is designed to automatically generate repository files

## Installation

```
go get "gitlab.com/golight/orm"
```

## Description

```go
//building genstorage

run build.sh
```

```go
//parsing file for AST
func ReflectFile(fileName string) ([]ReflectData, error)
```
```go
//generate methods
func GenerateMethods(filename string) error
func generateMethods(data ReflectData) string
```
```go
//checking for methods
func methodExists(methods []MethodInfo, structName, methodName string) bool
```
```go
//append the generated code to the end of the file
func appendToFile(filename, content string) error
```
## Usage

```go
//parsing file name
filename, err = genstorage.GetFileName()
//generate methods from the file
err = GenerateMethods(filename)
//generate the new storage files from the file
storage, err = genstorage.NewStorage(filename)
err = storage.CreateStorageFiles()
```

## Roadmap
- Tests for GetPackageName, NewStorage, NewStorageTemplate, NewInterfaceTemplate.